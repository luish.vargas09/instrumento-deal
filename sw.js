/* eslint-disable no-restricted-globals */
// importScripts('js/sw-utils');
const CACHE_STATIC = "static-v1.1";
const CACHE_INMUTABLE = "inmutable-v1.1";
const CACHE_DYNAMIC = "dynamic-v1.1";

function limpiarCache(cacheName, numeroItem) {
	//Agregar función debajo del put en dynamic  -> limpiarCache(CACHE_DYNAMIC,5)
	caches.open(cacheName).then((cache) => {
		return cache.keys().then((keys) => {
			if (keys.length > numeroItem) {
				cache.delete(keys[0]).then(limpiarCache(cacheName, numeroItem));
			}
		});
	});
}


self.addEventListener("install", (event) => {
	const cacheStatico = caches.open(CACHE_STATIC).then((cache) => {
		//return cache.addAll(["/", "index.html", "js/app.js", "js/controller/homeController.js", "js/view/homeView.js", "img/logo.png", "css/home.css", "manifest.json", "helper/sesionHelper.js"]);
		return cache.addAll([
			"/",
			"index.html",
			"css/madurez.css",
			"css/home.css",
			"css/inicioSesion.css",
			"css/login.css",
			"css/main.css",
			"css/registro.css",
			"js/controller/homeController.js",
			"js/controller/inicioSesionControler.js",
			"js/controller/loginController.js",
			"js/controller/madurezController.js",
			"js/controller/registroController.js",

			"js/model/homeModel.js",
			"js/model/inicioSesionModel.js",
			"js/model/loginModel.js",
			"js/model/madurezModel.js",
			"js/model/registroModel.js",

			"js/view/homeView.js",
			"js/view/inicioSesionView.js",
			"js/view/loginView.js",
			"js/view/madurezView.js",
			"js/view/registroView.js",

			"js/app.js",
			"lib/chart.js",
		]);
	});
	const cacheInmutable = caches.open(CACHE_INMUTABLE).then((cache) => {
		return cache.addAll([
			"bootstrap/css/bootstrap.min.css",
			"bootstrap/js/bootstrap.min.js",
		]);
	});
	event.waitUntil(Promise.all([cacheStatico, cacheInmutable]));
});

//Cache dinámico

// self.addEventListener("fetch", (event) => {
// 	// Si encuentra algo en el cache lo regresa y guarda ese recurso
// 	const recurso = caches.match(event.request).then((resp) => {
// 		if (resp) return resp;

// 		// No existe el archivo... tengo que ir a la web >:(
// 		// console.log(
// 		// 	"No existe... tendré que ir a esta dirección: \n",
// 		// 	event.request.url
// 		// );

// 		// Hago un fecth a esa dirección para obtener el recurso
// 		return fetch(event.request.url).then((newResp) => {
// 			// El cache dinámico puede crecer demasiado
// 			caches.open(CACHE_DYNAMIC).then((cache) => {
// 				cache.put(event.request.url, newResp);
// 			});
// 			return newResp.clone();
// 		});
// 	});
// 	event.respondWith(recurso);
// });
