import { homeController } from "./homeController.js";
import { saveMadurez } from "../model/madurezModel.js";
import { madurezView } from "../view/madurezView.js";
import { loginController } from "./loginController.js";

let body;
let preguntasDiv;
let respuestasDiv;
let i;
let salirButton;
let usuario;
let uid;

let preguntas = [
	{
		pregunta: "¿Cuentas con un control de inventario o catálogo de servicios?",
		dimension: 1,
		opciones: [
			{
				respuesta: "Sí",
				valor: 1.5,
			},
			{
				respuesta: "No",
				valor: 0,
			},
		],
	},
	{
		pregunta: "¿Cuentas con una gestión de crédito(s) o cobranza?",
		dimension: 1,
		opciones: [
			{
				respuesta: "Sí",
				valor: 1.5,
			},
			{
				respuesta: "No",
				valor: 0,
			},
		],
	},
	{
		pregunta: "¿Cuentas con contabilidad electrónica?",
		dimension: 1,
		opciones: [
			{
				respuesta: "Sí",
				valor: 1.5,
			},
			{
				respuesta: "No",
				valor: 0,
			},
		],
	},
	{
		pregunta: "¿Cuentas con nóminas?",
		dimension: 1,
		opciones: [
			{
				respuesta: "Sí",
				valor: 1.5,
			},
			{
				respuesta: "No",
				valor: 0,
			},
		],
	},
	{
		pregunta: "¿Cuentas con facturacion?",
		dimension: 1,
		opciones: [
			{
				respuesta: "Sí",
				valor: 1.5,
			},
			{
				respuesta: "No",
				valor: 0,
			},
		],
	},
	{
		pregunta: "¿Cuentas con un punto de venta?",
		dimension: 1,
		opciones: [
			{
				respuesta: "Sí",
				valor: 1.5,
			},
			{
				respuesta: "No",
				valor: 0,
			},
		],
	},
	{
		pregunta: "¿Cuentas con una estrategia de administración de proyectos?",
		dimension: 1,
		opciones: [
			{
				respuesta: "Sí",
				valor: 1.5,
			},
			{
				respuesta: "No",
				valor: 0,
			},
		],
	},
	{
		pregunta: "¿Cuentas con control de calidad?",
		dimension: 1,
		opciones: [
			{
				respuesta: "Sí",
				valor: 1.5,
			},
			{
				respuesta: "No",
				valor: 0,
			},
		],
	},
	{
		pregunta: "¿Cuentas con control de procesos o manufactura?",
		dimension: 1,
		opciones: [
			{
				respuesta: "Sí",
				valor: 1.5,
			},
			{
				respuesta: "No",
				valor: 0,
			},
		],
	},
	{
		pregunta: "¿Realizas una gestión de relación con los clientes?",
		dimension: 1,
		opciones: [
			{
				respuesta: "Sí",
				valor: 1.5,
			},
			{
				respuesta: "No",
				valor: 0,
			},
		],
	},
	{
		pregunta: "¿Anuncias tus productos en internet?",
		dimension: 2,
		opciones: [
			{
				respuesta: "Sí",
				valor: 1.472,
			},
			{
				respuesta: "No",
				valor: 0,
			},
		],
	},
	{
		pregunta: "¿Utilizas alguna aplicación para apoyar tus procesos de comercialización?",
		dimension: 2,
		opciones: [
			{
				respuesta: "Sí",
				valor: 3.04,
			},
			{
				respuesta: "No",
				valor: 0,
			},
		],
	},
	{
		pregunta: "¿Cierras la venta de tus productos por chat en internet?",
		dimension: 2,
		opciones: [
			{
				respuesta: "Sí",
				valor: 4.576,
			},
			{
				respuesta: "No",
				valor: 0,
			},
		],
	},
	{
		pregunta: "¿Cierras la venta de tus productos mediante una plataforma digital propia?",
		dimension: 2,
		opciones: [
			{
				respuesta: "Sí",
				valor: 7.616,
			},
			{
				respuesta: "No",
				valor: 0,
			},
		],
	},
	{
		pregunta: "¿Cuál es la principal propuesta de valor de la marca o el producto?",
		dimension: 5,
		opciones: [
			{
				respuesta: "No se tiene identificada",
				valor: 0,
			},
			{
				respuesta: "Precio",
				valor: 1.512,
			},
			{
				respuesta: "Calidad",
				valor: 3,
			},
			{
				respuesta: "Productos innovadores",
				valor: 4.512,
			},
			{
				respuesta: "Productos diferenciados",
				valor: 6,
			},
			{
				respuesta: "Productos únicos en el mercado y/o que nadie más ofrece",
				valor: 7.488,
			},
		],
	},
	{
		pregunta: "Indica cuál de las siguientes afirmaciones es la que aplica en tu marca",
		dimension: 5,
		opciones: [
			{
				respuesta: "No tenemos una marca",
				valor: 0,
			},
			{
				respuesta: "La marca es nueva y/o aún no es conocida",
				valor: 1.512,
			},
			{
				respuesta: "Aunque tiene tiempo, la marca aún no es reconocida",
				valor: 3,
			},
			{
				respuesta: "La marca tiene algunos seguidores",
				valor: 4.512,
			},
			{
				respuesta: "La marca es ampliamente reconocida, pero no es líder",
				valor: 6,
			},
			{
				respuesta: "La marca es líder en el mercado regional",
				valor: 7.512,
			},
			{
				respuesta: "La marca es líder en el mercado nacional",
				valor: 9,
			},
			{
				respuesta: "La marca es reconocida a nivel internacional",
				valor: 10.512,
			},
		],
	},
	{
		pregunta: "¿Sabes quiénes son tus clientes?",
		dimension: 5,
		opciones: [
			{
				respuesta: "Realmente no lo sé",
				valor: 0,
			},
			{
				respuesta: "Tengo una ligera idea",
				valor: 1.512,
			},
			{
				respuesta: "Tengo una buena idea",
				valor: 3,
			},
			{
				respuesta: "Lo sé con certeza",
				valor: 4.512,
			},
			{
				respuesta: "Tomamos acciones para monitorearlo y saberlo",
				valor: 6,
			},
		],
	},
	{
		pregunta: "¿Realizas una inversión en marketing digital?",
		dimension: 2,
		opciones: [
			{
				respuesta: "No se hace ninguna inversión",
				valor: 0,
			},
			{
				respuesta: "Se hacen inversiones ocasionales",
				valor: 1.536,
			},
			{
				respuesta: "Se hacen inversiones constantes",
				valor: 3.04,
			},
			{
				respuesta: "Tenemos una partida y un plan anual de Marketing Digital a nivel nacional",
				valor: 4.576,
			},
			{
				respuesta: "Tenemos una partida y un plan anual de Marketing Digital a nivel nacional e internacional",
				valor: 6.08,
			},
		],
	},
	{
		pregunta: "¿Las ventas en línea las maneja una agencia externa?",
		dimension: 2,
		opciones: [
			{
				respuesta: "Sí",
				valor: 1.536,
			},
			{
				respuesta: "No",
				valor: 0,
			},
		],
	},
	{
		pregunta: "¿Cuentas con personal especializado en ventas en línea?",
		dimension: 2,
		opciones: [
			{
				respuesta: "Sí",
				valor: 1.536,
			},
			{
				respuesta: "No",
				valor: 0,
			},
		],
	},
	{
		pregunta: "¿Cuentas con personal bilingüe de atención al cliente?",
		dimension: 2,
		opciones: [
			{
				respuesta: "Sí",
				valor: 1.536,
			},
			{
				respuesta: "No",
				valor: 0,
			},
		],
	},
	{
		pregunta: "¿Cuentas con personal interno de comercio exterior?",
		dimension: 2,
		opciones: [
			{
				respuesta: "Sí",
				valor: 1.536,
			},
			{
				respuesta: "No",
				valor: 0,
			},
		],
	},
	{
		pregunta: "¿Cuentas con personal interno especializado para Marketing Digital?",
		dimension: 2,
		opciones: [
			{
				respuesta: "Sí",
				valor: 1.536,
			},
			{
				respuesta: "No",
				valor: 0,
			},
		],
	},
	{
		pregunta: "¿Cuentas con un plan permanente de capacitación en temas digitales?",
		dimension: 2,
		opciones: [
			{
				respuesta: "Sí",
				valor: 1.536,
			},
			{
				respuesta: "No",
				valor: 0,
			},
		],
	},
	{
		pregunta: "¿Estás asociado con una empresa de paquetería (UPS, FedEx, DHL, Estafeta, entre otros)?",
		dimension: 3,
		opciones: [
			{
				respuesta: "Sí",
				valor: 1.44,
			},
			{
				respuesta: "No",
				valor: 0,
			},
		],
	},
	{
		pregunta: "¿Cuentas con distribuidores en el extranjero?",
		dimension: 3,
		opciones: [
			{
				respuesta: "Sí",
				valor: 1.456,
			},
			{
				respuesta: "No",
				valor: 0,
			},
		],
	},
	{
		pregunta: "¿Cuentas con un agente aduanal?",
		dimension: 3,
		opciones: [
			{
				respuesta: "Sí",
				valor: 1.456,
			},
			{
				respuesta: "No",
				valor: 0,
			},
		],
	},
	{
		pregunta: "¿Cuentas algún otro prestador de servicio para la comercialización?",
		dimension: 3,
		opciones: [
			{
				respuesta: "Sí",
				valor: 1.456,
			},
			{
				respuesta: "No",
				valor: 0,
			},
		],
	},
	{
		pregunta: "¿Cuentas con procesos de seguimiento de pedidos digitales?",
		dimension: 3,
		opciones: [
			{
				respuesta: "Sí",
				valor: 1.456,
			},
			{
				respuesta: "No",
				valor: 0,
			},
		],
	},
	{
		pregunta: "¿Cuentas con procesos de atención al cliente en línea?",
		dimension: 3,
		opciones: [
			{
				respuesta: "Sí",
				valor: 1.456,
			},
			{
				respuesta: "No",
				valor: 0,
			},
		],
	},
	{
		pregunta: "¿Cuentas con procesos para envíos?",
		dimension: 3,
		opciones: [
			{
				respuesta: "Sí",
				valor: 1.456,
			},
			{
				respuesta: "No",
				valor: 0,
			},
		],
	},
	{
		pregunta: "¿Cuentas con plataformas seguras de pago para pedidos digitales?",
		dimension: 3,
		opciones: [
			{
				respuesta: "Sí",
				valor: 1.456,
			},
			{
				respuesta: "No",
				valor: 0,
			},
		],
	},
	{
		pregunta: "¿Cuentas con un proceso de devolución de pedidos digitales?",
		dimension: 3,
		opciones: [
			{
				respuesta: "Sí",
				valor: 1.456,
			},
			{
				respuesta: "No",
				valor: 0,
			},
		],
	},
	{
		pregunta: "¿Cuentas con políticas de venta en línea?",
		dimension: 3,
		opciones: [
			{
				respuesta: "Sí",
				valor: 1.456,
			},
			{
				respuesta: "No",
				valor: 0,
			},
		],
	},
	{
		pregunta: "¿Cuentas con términos y condiciones de venta en línea?",
		dimension: 3,
		opciones: [
			{
				respuesta: "Sí",
				valor: 1.456,
			},
			{
				respuesta: "No",
				valor: 0,
			},
		],
	},
	{
		pregunta: "¿Cuentas con pagos en efectivo/cheques?",
		dimension: 4,
		opciones: [
			{
				respuesta: "Sí",
				valor: 0,
			},
			{
				respuesta: "No",
				valor: 0,
			},
		],
	},
	{
		pregunta: "¿Cuentas con pagos a través de PayPal?",
		dimension: 4,
		opciones: [
			{
				respuesta: "Sí",
				valor: 1.625,
			},
			{
				respuesta: "No",
				valor: 0,
			},
		],
	},
	{
		pregunta: "¿Cuentas con pagos mediante tarjetas de crédito/débito?",
		dimension: 4,
		opciones: [
			{
				respuesta: "Sí",
				valor: 1.625,
			},
			{
				respuesta: "No",
				valor: 0,
			},
		],
	},
	{
		pregunta: "¿Cuentas con pagos cargo contra entrega?",
		dimension: 4,
		opciones: [
			{
				respuesta: "Sí",
				valor: 1.625,
			},
			{
				respuesta: "No",
				valor: 0,
			},
		],
	},
	{
		pregunta: "¿Cuentas con pagos a través del móvil (NFC) (Apple Pay, Samsung Pay, Google Pay)?",
		dimension: 4,
		opciones: [
			{
				respuesta: "Sí",
				valor: 1.625,
			},
			{
				respuesta: "No",
				valor: 0,
			},
		],
	},
	{
		pregunta: "¿Cuentas con pagos mediante otras Apps de pago como MercadoPago?",
		dimension: 4,
		opciones: [
			{
				respuesta: "Sí",
				valor: 1.625,
			},
			{
				respuesta: "No",
				valor: 0,
			},
		],
	},
	{
		pregunta: "¿Cuentas con pagos mediante criptomonedas?",
		dimension: 4,
		opciones: [
			{
				respuesta: "Sí",
				valor: 1.625,
			},
			{
				respuesta: "No",
				valor: 0,
			},
		],
	},
	{
		pregunta: "¿Cuentas con pagos mediante transferencias bancarias y/o SPEI?",
		dimension: 4,
		opciones: [
			{
				respuesta: "Sí",
				valor: 1.625,
			},
			{
				respuesta: "No",
				valor: 0,
			},
		],
	},
	{
		pregunta: "¿Cuentas con pagos mediante otros sistemas de pago digitales?",
		dimension: 4,
		opciones: [
			{
				respuesta: "Sí",
				valor: 1.625,
			},
			{
				respuesta: "No",
				valor: 0,
			},
		],
	},
];

export const madurezController = () => {
	body = document.body;
	body.innerHTML = madurezView();
	i = 0;
	//
	preguntasDiv = document.getElementById("preguntasDiv");
	respuestasDiv = document.getElementById("respuestasDiv");
	salirButton = document.getElementById("salirButton");
	//
	usuario = JSON.parse(localStorage.getItem("usuario"));
	if (usuario == null) {
		localStorage.setItem("usuario", JSON.stringify(null));
		loginController();
	}

	preguntasConst(i);
	//
	salirButton.addEventListener("click", () => {
		homeController();
	});
};

let contador = {
	uid: "",
	total: 0,
	dimensiones: [
		{
			dimension: 1,
			valor: 0,
		},
		{
			dimension: 2,
			valor: 0,
		},
		{
			dimension: 3,
			valor: 0,
		},
		{
			dimension: 4,
			valor: 0,
		},
		{
			dimension: 5,
			valor: 0,
		},
	],
};

const preguntasConst = (j) => {
	let m = j + 1;
	preguntasDiv.innerHTML = `<h2 class="preguntaH2"><p style="font-size:16px;">${m}/44</p>${preguntas[j].pregunta}</h2>`;
	respuestasDiv.innerHTML = "";
	//
	let k = 0;
	preguntas[j].opciones.forEach((doc) => {
		respuestasDiv.innerHTML += `
            <button id="respuestaButton${k}" class="btn respuestaButton">${doc.respuesta}</button>    
        `;
		k++;
	});
	//
	k = 0;
	preguntas[j].opciones.map((doc) => {
		let respuestaButton = document.getElementById(`respuestaButton${k}`);
		k++;
		respuestaButton.addEventListener("click", () => {
			preguntasConst2(preguntas[j].dimension, doc.valor);
		});
	});
};

const preguntasConst2 = (dimension, valor) => {
	contador.total = contador.total + valor;
	contador.dimensiones.forEach((doc) => {
		if (doc.dimension == dimension) {
			doc.valor = doc.valor + valor;
		}
	});
	i++;
	if (i < preguntas.length) {
		preguntasConst(i);
	} else {
		preguntasDiv.innerHTML = ``;
		respuestasDiv.innerHTML = ``;
		//Guardar en BD
		contador.uid = usuario.uid;

		preguntasDiv.innerHTML = `
		<center>
            <div class="spinner-border spinnerDiv" role="status"></div>
        </center>
		`;
		salirButton.style.display = "none";
		respuestasDiv.innerHTML = "";

		console.log(contador);
		saveMadurez(contador);
		//
	}

	//agregar un IF limitador que cheque el array
};
