export const registroView = () => {
	return `
    <link rel="stylesheet" href="/css/registro.css">
    <div class="seccionDiv">
    	<div class="seccionDiv2">
    		<form id="registroForm">
    			<input id="nombreEmpresa" name="nombreEmpresa" class="btn" placeholder="Nombre de la empresa" />
    			<br />
    			<br />
    			<input id="correo" name="correo" class="btn" placeholder="Correo" />
    			<br />
    			<br />
    			<input id="contrasena" name="contrasena" class="btn" type="password" placeholder="Contraseña" />
    			<br />
    			<br />
    			<input id="confirmarContrasena" name="confirmarContrasena" class="btn" type="password" placeholder="Confirmar contraseña" />
    			<br />
    			<br />
    			<input class="btn registrarseInput" type="submit" value="Registrarse" data-bs-toggle="modal" data-bs-target="#modalDiv" />
    		</form>
    		<button id="iniciaSesionButton" class="btn iniciaSesionButton">Inicia sesión</button>
    	</div>
    </div>
    <!--Modal-->
    <div class="modal fade" id="modalDiv" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-hidden="true">
    	<div class="modal-dialog">
    		<div class="modal-content backgroundColor">
    			<div id="modalBodyDiv" class="modal-body"></div>
    			<div id="modalFooterDiv" class="modal-footer">
    				<button type="button" class="btn" data-bs-dismiss="modal">Ok</button>
    			</div>
    		</div>
    	</div>
    </div>
    `;
};
