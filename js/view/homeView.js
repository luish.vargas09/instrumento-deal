export const homeView = () => {
    return `
    <link rel="stylesheet" href="/css/home.css" />
    <button id="cerrarSesionButton" class="btn cerrarSesionButton">Cerrar sesión⛔</button>
    <div class="empresaDiv">
        <h1 id="empresaH1" class="empresaH1">EMPRESA!</h1>
    </div>
    <canvas id="madurezCanva" class="madurezCanva"></canvas>
    <canvas id="areasCanva" class="areasCanva"></canvas>
    <button id="medicionButton" class="btn medicionButton">Realizar medición de madurez digital</button>
    `;
};
