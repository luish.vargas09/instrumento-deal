export const loginView = () => {
	return `
    <link rel="stylesheet" href="/css/login.css" />
    <div id="navDiv" class="navDiv">
        <div>
            <button id="iniciaSesionButton" class="btn iniciaSesionButton">Inicia sesión</button>
            <button id="registrateButton" class="btn registrateButton">Regístrate</button>
        </div>
    </div>
    <div class="div1 div3 inicioDiv">
        <h1>Adoptar la economía digital.</h1>
    </div>
    <div class="div1 div2">
        <h1>Características de la economía digital.</h1>
    </div>
    <div class="div1 div3">
        <h1>Cambio de paradigma empresarial.</h1>
    </div>
    <div class="div1 div2">
        <h1>Cambio en la naturaleza del trabajo.</h1>
    </div>
    <div class="div1 div3">
        <h1>Transformación de industrias, nuevo comercio minorista, logística inteligente, nuevas finanzas y fabricación inteligente.</h1>
    </div>
    `;
};
