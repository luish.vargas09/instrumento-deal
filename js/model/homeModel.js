import { getDatos } from "../controller/homeController.js";

export const getMadurez = (uid) => {
	(async () => {
		console.log(uid);
		let returnLet = true;
		let arrayDatos;
		let doc = await firebase
			.firestore()
			.collection("madurez")
			.orderBy("createdAt")
			.get();

		doc.forEach((doc) => {
			if (doc.data().uid == uid) {
				console.log(doc.data());
				arrayDatos = doc.data();
				returnLet = false;
			}
		});

		if (returnLet) {
			getDatos(null);
			return;
		}
		//

		console.log(arrayDatos);
		localStorage.setItem(
			"madurez",
			JSON.stringify({
				uid: uid,
				total: arrayDatos.total,
				dimensiones: arrayDatos.dimensiones,
				//createdAt: firebase.firestore.FieldValue.serverTimestamp(),
			})
		);

		//let params = doc.docs[0];
		getDatos(arrayDatos);
	})().catch(() => {
		getDatos(null);
	});
};
