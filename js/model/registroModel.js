import { madurezController } from "../controller/madurezController.js";

export const crearUsuarioModel = (params) => {
	(async () => {
		console.log("Inicia");
		let doc = await firebase.auth().createUserWithEmailAndPassword(params.correo, params.contrasena);
		await firebase.firestore().collection("usuarios").doc().set({
			uid: doc.user.uid,
			correo: params.correo,
			nombreEmpresa: params.nombreEmpresa,
			fechaCrecion: firebase.firestore.FieldValue.serverTimestamp(),
		});
		console.log("Termina");
		localStorage.setItem(
			"usuario",
			JSON.stringify({
				uid: doc.user.uid,
				correo: params.correo,
				nombreEmpresa: params.nombreEmpresa,
				primerRegistro: true,
			})
		);
		location.reload();
		//madurezController();
	})().catch(() => {});
};
